import { installQuasar } from '@quasar/quasar-app-extension-testing-unit-vitest'
import { mount } from '@vue/test-utils'
import HeaderSection from 'src/components/mxv/HeaderSection.vue'
import { describe, expect, it } from 'vitest'

installQuasar()

describe('FORM SECTION TITLE', () => {
  it('Has Title & BgColor Props', () => {
    // Mount component
    const comp = mount(HeaderSection, {
      props: {
        title: 'Title',
        bgColor: 'green',
      },
    })

    expect(comp.text()).toContain('Title')
    expect(comp.classes()).toContain('bgcolor-' + comp.props().bgColor)
    expect(comp.classes()).not.toContain('text-overline')
  })

  it('Has Multiple Paragraphs as Body Props', () => {
    // Mount component
    const comp = mount(HeaderSection, {
      props: {
        title: 'Title',
        bgColor: 'green',
        body: ['Paragraph 1', 'Paragraph 2', 'Paragraph 3'],
      },
    })

    const body = comp.props('body')

    expect(body).toHaveLength(3)

    body.push('This is last p')

    expect(body).toHaveLength(4)
  })
})

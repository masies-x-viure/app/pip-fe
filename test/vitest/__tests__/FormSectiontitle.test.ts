import { installQuasar } from '@quasar/quasar-app-extension-testing-unit-vitest'
import { mount } from '@vue/test-utils'
import FormSectionTitle from 'src/components/mxv/FormSectionTitle.vue'
import { describe, expect, it } from 'vitest'

installQuasar()

describe('FORM SECTION TITLE', () => {
  it('Has Title & Subtitle Props', () => {
    // Mount component
    const comp = mount(FormSectionTitle, {
      props: {
        title: 'Title',
        subtitle: 'Subtitle',
      },
    })

    expect(comp.text()).toContain('Title')
    expect(comp.text()).toContain('Subtitle')
  })

  it('Has NOT Title & Subtitle Props', () => {
    // Mount component
    const comp = mount(FormSectionTitle, {
      props: {},
    })

    expect(comp.text()).not.toContain('Title')
    expect(comp.props().title).toBe(undefined)
    expect(comp.props().subtitle).toBe(undefined)
    expect(comp.text()).not.toContain('Subtitle')
  })
})

// describe('COMPONENT -- Form Section Title', () => {
//   it('Should have missing title & subtitle', () => {
//     const comp = mount(FormSectionTitle)

//     // ASSERTION
//     expect(comp.innerText()).toBe('t')
//   })

//   it('Should have title & subtitle', () => {
//     const comp = mount(FormSectionTitle, {
//       props: {
//         title: 'Title',
//         subtitle: 'Subtitle',
//       },
//     })

//     console.log('=>>>>>>>  ', comp.innerText())

//     // ASSERTION
//     expect(comp.text()).not.toBe('Title')
//     expect(comp.text()).toBe('Subtitle')
//   })
// })

import { createTestingPinia } from '@pinia/testing'
import { createPinia, defineStore, setActivePinia } from 'pinia'
import { installQuasar } from '@quasar/quasar-app-extension-testing-unit-vitest'
import { mount, shallowMount } from '@vue/test-utils'

import { beforeEach, describe, expect, it, test } from 'vitest'

import { useUCStore } from 'src/stores/UCprofile'
import CardProfile from 'src/components/mxv/CardProfile.vue'
import { forEach } from 'cypress/types/lodash'
installQuasar()

const mockProject = {
  name: 'Mock Project',
  descriptionShort: 'Mock short description',
  locations: ['La Garrotxa', ' Maresme'],
  purposes: ['Jugar', 'Caminar'],
}

const mockUnderAge = {
  adults: 2,
  underage: 3,
}

const mockMembers = [
  { name: 'Mock Member 1', birthDate: new Date() },
  { name: 'Mock Member 2', birthDate: new Date() },
]

const mockPhotos = [
  'https://example.com/photo1.jpg',
  'https://example.com/photo2.jpg',
]

/**
 * Initial Central state
 */
const state = {
  adultsAndUnderage: { adults: 2, underage: 3 },
  project: mockProject,
  members: mockMembers,
  photos: mockPhotos,
}

/**
 * TESTS START
 */

describe('CardProfile - Component ---', () => {
  let comp: any = null

  // SETUP - run prior to each unit test
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  afterEach(() => {})

  /**
   * TESTS
   */

  test('Project has no Name', () => {
    const store = useUCStore()
    const comp = mount(CardProfile)

    // Assertion
    expect(comp.find('[data-project-name]').exists()).toBe(false)
  })

  test('Project has a Name ', () => {
    // Setup
    const store = useUCStore()
    store.$state.project = mockProject

    const comp = mount(CardProfile)

    // Assertion
    expect(comp.find('[data-project-name]').exists()).toBe(true)
  })

  test('Suggest to add msgs', () => {
    // Setup
    const store = useUCStore()
    const comp = mount(CardProfile)
    const expectedText = 'Millor si poseu una foto vostra'

    // Assertion
    expect(comp.find('[data-photos-msg]').text()).toContain(expectedText)
    expect(comp.find('[data-photos-msg]').exists()).toBe(true)
  })

  test('displays member names if project name is not available', () => {
    // SETUP
    const store = useUCStore()
    store.$patch({
      members: mockMembers,
    })

    const comp = mount(CardProfile)

    // Assertion
    expect(comp.find('[data-members]').exists()).toBe(true)
    expect(comp.findAll('[data-members]').length).toBe(2)

    mockMembers.forEach((member) => {
      expect(comp.html()).toContain(member.name)
    })
  })

  test('displays Carousel if photos are available', () => {
    // Setup
    const store = useUCStore()
    store.$patch({
      photos: mockPhotos,
    })

    const comp = mount(CardProfile)

    // Assertion
    expect(comp.findComponent({ name: 'Carousel' }).exists()).toBe(true)
  })
})

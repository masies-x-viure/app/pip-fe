# MxV - Persones i Projectes (PiP)

Aplicació web per enxarxar les persones que vulguin habitar i produir en convivència amb la natura i les societats per a què siguin protagonistes del canvi de model productiu i habitacional.

> El Projecte MxV serà format per 4 repositoris:

- Persones i Projectes (PiP) **En desenvolupament**

Més endavant

- Professionals
- Habitatge, Terres i Espais (HTE)
- Eines d'Economia Social i Solidària (EESS)

## Install the dependencies

```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Lint the files

```bash
yarn lint
# or
npm run lint
```

### Format the files

```bash
yarn format
# or
npm run format
```

### Build the app for production

```bash
quasar build
```

### Customize the configuration

See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).

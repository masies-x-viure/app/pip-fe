import { date } from 'quasar'
import { defineStore } from 'pinia'
import { UCForm, Member } from 'src/components/models'
import { api } from 'src/boot/axios'

interface Ages {
  adults: number
  underage: number
}

/**
 * Get the age of a member by his BirthDate
 * @param member
 * @returns number
 */
export const getAge = (member: Member): number => {
  const today = new Date()
  const todaysMonth: number = today.getMonth() + 1
  const todaysDay: number = today.getDate()

  let birthDate = member.birthDate

  const birthMonth: number = birthDate.getMonth() + 1
  const birthDay: number = birthDate.getDate()

  if (birthMonth > todaysMonth) {
    birthDate = date.addToDate(birthDate, { years: 1 })
  }

  if (birthMonth === todaysMonth && birthDay < todaysDay) {
    birthDate = date.addToDate(birthDate, { years: 1 })
  }
  const age = date.getDateDiff(today, birthDate, 'years')

  return age
}

export const useUCStore = defineStore('UCprofile', {
  state: (): UCForm => ({
    members: [],
    project: {
      name: '',
      descriptionShort: '',
      locations: [],
      purposes: [],
    },
    photos: [],
  }),
  getters: {
    adultsAndUnderage: (state): Ages => {
      const ages: Ages = {
        adults: 0,
        underage: 0,
      }

      state.members.forEach((member) => {
        const age = getAge(member)

        age > 17 ? ages.adults++ : ages.underage++
      })

      return ages
    },
  },
  actions: {
    async creatUC() {
      const data: object = {
        data: {
          members: this.members,
          projects: [this.project],
        },
      }

      try {
        const resp = await api.post('/api/ucs?populate=*', data)
        return resp
      } catch (error) {
        console.error(error)
      }
    },

    /**
     * Get All Guilds
     * @returns string[]: array of Guilds
     */
    async getAllGuilds() {
      try {
        const resp = await api.get('/api/ofici?populate=*')
        return resp.data
      } catch (error) {
        console.error(error)
      }
    },

    /**
     * Get Locations: County, Municipality & Province
     * @returns string[]: array of Locations
     */
    async getAllLocations() {
      try {
        const resp = await (await api.get('/api/locations-cat')).data
        return resp.data.attributes
      } catch (error) {
        console.error(error)
      }
    },

    /**
     * Get All Purposes a project can be for
     * @returns string[]: array of Purposes
     */
    async getAllPurposes() {
      try {
        const resp = await (await api.get('/api/purpose?populate=*')).data

        return resp.data.attributes.purposes
      } catch (error) {
        console.error(error)
      }
    },
  },
})

export interface Member {
  name: string
  birthDate: Date
  guild?: string
  gender: 'male' | 'female' | 'transgender'
  location?: string
}

export interface Project {
  name: string
  descriptionShort: string
  description?: string
  locations: string[]
  purposes: string[]
}

export interface UCForm {
  members: Member[]
  photos: string[]
  project: Project
}
